--This class, when enabled, will ensure that connections are valid from the server to the client.

local connection = {}
connection.__index = connection

local function new(ip, port)
  return setmetatable({ip = ip, port = port, handlers = {}, status = "", oldStatus = ""}, connection)
end

function connection:connect()
	if self.status ~= "connected" then
		self.outbound = {}
		self.status = "connecting"
		local socket = require('socket')
		self.client = socket.connect(self.ip, self.port)
		if self.client ~= nil then
			self.client:settimeout(0)
			self.status = "connected"
		end
	end
end

function connection:receive()
	line, err = self.client:receive()
	if err == "closed" then
		self.client:close()
		self.status = "disconnected"
	elseif err ~= "timeout" then
		local data = explode("|", line)
		local queue = data[1]
		local message = data[2]
		self.handlers[queue](message)
	end
end



-- The following functions are intended to be used by the user.
function connection:send(infoString, queue)
	queue = queue .. "|"
	local message = queue .. infoString .. "\n"
	table.insert(self.outbound, message)
end

function connection:registerHandler(func, queue)
	self.handlers[queue] = func
end

function connection:removeHandler(queue)
	self.handlers[queue] = nil
end

function connection:clearSendQueue()
	self.outbound = {}
end

function connection:disconnect()
	self.client:close()
	self.status = "disconnected"
end

function connection:update(dt)
	self.oldStatus = self.status
	if self.status == "connected" then
		self:receive()
		for k,v in ipairs(self.outbound) do
			self.client:send(v)
		end
		self.outbound = {}
	else
		self:connect()
	end
	if self.oldStatus ~= self.status then
		print(self.status )
	end
end


return setmetatable({new = new},
  
  {__call = function(_, ...) return new(...) end})
