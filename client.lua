
require 'helpers'
local json =loadfile('lib/json.lua')() 
function love.load(arg)
    connection = require 'client'
    c = connection("127.0.0.1", 8888)
    c:registerHandler(function(data) enemy = data end, "play")
    c:registerHandler(function(data) model = data end, "turn")
    model = {0}
end

function love.update(dt)
    c:update(dt)
end
function love.draw()
    love.graphics.print("client "..name,0,0)
    love.graphics.print("model "..model[1],0,20)
    if enemy then
        love.graphics.print("enemy "..enemy,0,40)
    end

end
function love.keypressed(key)
    if key == "a" then
        c:send(json:encode({name = name}), "player")
    elseif key == "s" then
        c:send(json:encode({name = name,model = model}), "turn")
    elseif key == "d" then
        model[1] = model[1] + 1
    elseif key == "c" then
        model[1] = model[1] - 1
    end
end
