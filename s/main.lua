require('helpers')
local json =loadfile('lib/json.lua')() 
local function parse(data)
    local info = explode("|",data)
    local client,msg = info[1],info[2]
    return client,json:decode(msg)
end
local function register(data)
    local client, msg = parse(data)
    if not room[1] then
        room[1] = {}
        room[1].id = client
        room[1].name = msg.name
    elseif not room[2] and client ~= room[1].id then
        room[2] = {}
        room[2].id = client
        room[2].name = msg.name
    end
end
local function turn(data)
    local client, msg = parse(data)
    if client == currentclient.id then
        if currentclient.id == room[1].id then
            currentclient = room[2]
        else
            currentclient = room[1]
        end
        model = msg.model
        model.current = currentclient.name
        s:send(room[1].id,json:encode(model),"turn")
        s:send(room[2].id,json:encode(model),"turn")
    end
end
local function move(data)
    local client, msg = parse(data)
    
end
function love.load(arg)
    local ww,wh = love.window.getDesktopDimensions()
    love.window.setMode(ww/4,wh/4,{x = ww/2, y= wh/2})
    server = require 'server'
    s = server(8888, "*")
    room = {}
    s:registerHandler(register, "player")
    s:registerHandler(turn, "turn")
    s:registerHandler(move, "move")
end

function love.update(dt)
    if room[1] and room[2] and not currentclient then
        currentclient = {}
        currentclient =  room[1]
        s:send(room[1].id,room[2].name,"play") 
        s:send(room[2].id,room[1].name,"play")

    end
    s:update(dt)
end
function love.draw()
    love.graphics.print("server",0,0)
    if room[1] then 
        love.graphics.print(room[1].name,0,20)
    end
    if room[2] then
        love.graphics.print(room[2].name,0,40)
    end
    if currentclient then
        love.graphics.print("room "..currentclient.name,0,60)
    end
    if model then
        love.graphics.print("model "..model[1],0,80)
    end
end
