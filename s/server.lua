--This class, when enabled, will ensure that connections are valid from the server to the client.

local connection = {}
connection.__index = connection

local function new(port, ip)
  return setmetatable({ip = ip, port = port, clients = {}, handlers = {}, prevClients = 0, disconnectedClients = {}, outboundQueue = {}}, connection)
end

function connection:startServer()
	if self.status ~= "serving" then
		self.outbound = {}
		print("Starting Server")
		local socket = require('socket')
		self.server = socket.bind(self.ip, self.port)
		if self.server ~= nil then
			self.server:settimeout(0)
			self.status = "serving"
		end
	end
end

function connection:registerHandler(func, queue)
	self.handlers[queue] = func
end

function connection:removeHandler(queue)
	self.handlers[queue] = nil
end

function connection:send(client, infoString, queue)
	local temp = {}
	temp.client = client
	temp.infoString = infoString
	temp.queue = queue
	table.insert(self.outboundQueue, temp)
end

function connection:processOutboundQueue(clientID, clientSocket)
	for k, v in ipairs(self.outboundQueue) do
		if k == clientID then
			clientSocket:send(v.queue .. "|" .. v.infoString .. "\n")
		end
	end
end

function connection:receive(client, line, err)
	local data = explode("|", line)
	local queue = data[1]
	local message = data[2]
	if self.handlers[queue] ~= nil then
		self.handlers[queue](client .. "|" .. message)
	else
		print('No handler set to handle the call from the client.')
		print('Queue:')
		print(queue)
		print('Message:')
		print(client)
		print(message)
		assert('No handler provided')
	end
end

function connection:update(dt)
	self.prevClients = #self.clients
	if self.status == "serving" then
		local newClient = {}
		local disconnectedClients = {}
		newClient = self.server:accept()
		if newClient ~= nil then
			newClient:settimeout(0)
			table.insert(self.clients, newClient)
		end
		for k,v in ipairs(self.clients) do
			local line, err = v:receive()
			if err == "closed" then
				table.insert(self.disconnectedClients, k)
			elseif line ~= nil then
				self:receive(k, line, err)
			end
			self:processOutboundQueue(k, v)
		end
		self.outBoundQueue = {}
		for k,v in ipairs(self.disconnectedClients) do
			table.remove(self.clients, v)
		end
		self.disconnectedClients = {}
	else
		self:startServer()
	end
	if self.prevClients ~= #self.clients then
		print("Number of clients: " .. #self.clients)
	end
end

return setmetatable({new = new},
  
  {__call = function(_, ...) return new(...) end})
